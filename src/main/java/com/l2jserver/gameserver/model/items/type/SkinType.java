package com.l2jserver.gameserver.model.items.type;

public enum SkinType {
    NONE,
    EPIC,
    LEGENDARY,
    MYTHIC,
    ULTIMATE
}
