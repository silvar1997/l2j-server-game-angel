package com.l2jserver.gameserver.config;

import com.l2jserver.gameserver.config.converter.BalanceConverter;
import com.l2jserver.gameserver.model.actor.stat.CharStat;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.model.stats.Stats;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Mutable;

import java.util.HashMap;
import java.util.Map;

import static java.util.concurrent.TimeUnit.MINUTES;
import static org.aeonbits.owner.Config.HotReloadType.ASYNC;
import static org.aeonbits.owner.Config.LoadType.MERGE;

@Config.Sources({
        "file:${L2J_HOME}/custom/game/config/balance.properties",
        "file:./config/balance.properties",
        "classpath:config/balance.properties"
})
@Config.LoadPolicy(MERGE)
@Config.HotReload(value = 1, unit = MINUTES, type = ASYNC)
public interface BalanceConfiguration extends Mutable {

    Map<CharStat, Map<ClassId, Float>> classes = new HashMap<>();

    default Float getMultiplierForStat(int classId, Stats stat) {

        return switch (stat) {
            case BOW_MP_CONSUME_RATE -> getBowMpConsumeRate().get(classId);
            default -> throw new IllegalArgumentException();
        };

    }


    @Key("BOW_MP_CONSUME_RATE=")
    @ConverterClass(BalanceConverter.class)
    Map<Integer, Float> getBowMpConsumeRate();

}
