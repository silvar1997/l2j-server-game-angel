package com.l2jserver.gameserver.custom.images;

import com.l2jserver.gameserver.config.Configuration;
import com.l2jserver.gameserver.model.events.Containers;
import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.impl.IBaseEvent;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageManager {

    private final Map<Integer, BufferedImage> bufferedImages = new ConcurrentHashMap<>();
    private final Map<String, Integer> nameImageId = new ConcurrentHashMap<>();
//    private static int counter = 1000;

    private ImageManager() {
        reload();
        Containers.Players().addListener(new ConsumerEventListener(Containers.Players(), EventType.ON_PLAYER_LOGIN, this::sendImages, this));
    }

    public void reload() {
        File folder = new File(Configuration.server().getDatapackRoot() + "/data/custom/serverimages");
        File[] listOfFiles = folder.listFiles();
        assert listOfFiles != null;
        for (File f : listOfFiles) {
            if (!f.isFile()) {
                continue;
            }
            try {
                addImage(f.getName().substring(0, f.getName().lastIndexOf('.')), ImageIO.read(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void addImage(String imageName, BufferedImage image) {
        bufferedImages.put(image.hashCode(), image);
        nameImageId.put(imageName.toLowerCase(), image.hashCode());
    }

    public Optional<BufferedImage> getImage(String name) {
        return Optional.ofNullable(bufferedImages.get(nameImageId.get(name.toLowerCase())));
    }

    public int getImageId(String name) {
        return nameImageId.get(name.toLowerCase());
    }

    private void sendImages(IBaseEvent event) {
        OnPlayerLogin loginEvent = (OnPlayerLogin) event;
        bufferedImages.forEach((key, value) -> DDSConverter.sendImage(loginEvent.getActiveChar(), key, value));
    }

    public static ImageManager getInstance() {
        return ImageManager.SingletonHolder.instance;
    }

    private static class SingletonHolder {
        protected static final ImageManager instance = new ImageManager();
    }


}
